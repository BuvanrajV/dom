window.addEventListener("load", () => {
  const loader = document.querySelector(".loader");
  loader.classList.add("loader--hidden");
  loader.addEventListener("transitionend", () => {
    loader.remove();
  });
});

fetch("https://restcountries.com/v3.1/all")
  .then((data) => data.json())
  .then((countryDetails) => {
    appendToHtml(countryDetails);
  })
  .then(() => {
    let select = document.getElementById("select");
    select.addEventListener("click", filterCountries);
  })
  .catch((err) => {
    alert("Error is Occur ");
    console.log(err);
  });

function popupDetails(countryName) {
  fetch("https://restcountries.com/v3.1/all")
    .then((data) => data.json())
    .then((countryDetails) => {
      appendToHtml(countryDetails, countryName);
    });
}
function appendToHtml(countryDetails, countryName) {
  if (countryName === undefined) {
    let section = document.getElementById("countries-container");
    for (let countryInfo of countryDetails) {
      let newDiv = document.createElement("div");
      newDiv.className = "country-div";
      newDiv.setAttribute(
        "onclick",
        `popupDetails('${countryInfo.name.official}')`
      );
      section.appendChild(newDiv);

      let imageTag = document.createElement("img");
      imageTag.className = "country-flag";
      imageTag.setAttribute("src", countryInfo.flags.png);
      newDiv.appendChild(imageTag);

      let div1 = document.createElement("div");
      div1.className = "country-name";
      div1.innerText = countryInfo.name.official;
      newDiv.appendChild(div1);

      let div2 = document.createElement("div");
      let span1 = document.createElement("span");
      let span2 = document.createElement("span");
      div2.className = "country-population";
      span1.className = "country-span-title";
      span2.className = "country-span-detail";
      span1.innerText = "Population : ";
      span2.innerText = countryInfo.population;
      div2.appendChild(span1);
      div2.appendChild(span2);
      newDiv.appendChild(div2);

      let div3 = document.createElement("div");
      let span3 = document.createElement("span");
      let span4 = document.createElement("span");
      div3.className = "country-region";
      span3.className = "country-span-title";
      span4.className = "country-span-detail";
      span3.innerText = "Region : ";
      span4.innerText = countryInfo.region;
      div3.appendChild(span3);
      div3.appendChild(span4);
      newDiv.appendChild(div3);

      let div4 = document.createElement("div");
      let span5 = document.createElement("span");
      let span6 = document.createElement("span");
      div4.className = "country-capital";
      span5.className = "country-span-title";
      span6.className = "country-span-detail";
      span5.innerText = "Capital : ";
      span6.innerText = countryInfo.capital;
      div4.appendChild(span5);
      div4.appendChild(span6);
      newDiv.appendChild(div4);
    }
  } else {
    for (let countryInfo of countryDetails) {
      if (countryInfo.name.official == countryName) {
        let popupContainer = document.getElementById("popup-container");
        popupContainer.style.display = "block";
        let div = document.createElement("div");
        div.id = "popup-div";
        popupContainer.appendChild(div);

        let div5 = document.createElement("div");
        div.appendChild(div5);

        let imageTag = document.createElement("img");
        imageTag.className = "popup-country-flag";
        imageTag.setAttribute("src", countryInfo.flags.png);
        div5.appendChild(imageTag);

        let div6 = document.createElement("div");
        div6.className = "popup-content";
        div.appendChild(div6);

        let div7 = document.createElement("div");
        div7.textContent = countryInfo.name.official;
        div7.className = "popup-content";
        div6.appendChild(div7);

        let div8 = document.createElement("div");
        div8.textContent = "Population : " + countryInfo.population;
        div6.appendChild(div8);

        let div9 = document.createElement("div");
        div9.textContent =
          "Native Name : " +
          Object.values(countryInfo.name.nativeName)[0].official;
        div6.appendChild(div9);

        let div10 = document.createElement("div");
        div10.textContent = "Region : " + countryInfo.region;
        div6.appendChild(div10);

        let div11 = document.createElement("div");
        div11.textContent = "Sub Region : " + countryInfo.subregion;
        div6.appendChild(div11);

        let div12 = document.createElement("div");
        div12.textContent = "Capital : " + countryInfo.capital;
        div6.appendChild(div12);

        let div13 = document.createElement("div");
        div13.textContent =
          "Currencies : " + Object.values(countryInfo.currencies)[0].name;
        div6.appendChild(div13);

        let div14 = document.createElement("div");
        div14.textContent =
          "Languages : " + Object.values(countryInfo.languages);
        div6.appendChild(div14);

        let button = document.createElement("button");
        button.textContent = "Back";
        div6.appendChild(button);

        button.addEventListener("click", closePopup);

        document.addEventListener("keydown", function (event) {
          if (event.keyCode === 27) {
            closePopup();
          }
        });

        document.body.addEventListener('click',closePopup); 

        let countryContainer = document.getElementById("countries-container");
        countryContainer.className = "open-popup-opacity";
      }
    }
  }
}

function closePopup(event) {
  document.getElementById("popup-div").remove();
}

function filterCountries(e) {
  let value = e.target.value;
  let countryDiv = document.getElementsByClassName("country-div");
  for (let countryInfo of countryDiv) {
    region = countryInfo.childNodes[3].childNodes[1].textContent;
    if (value == "All-region") {
      countryInfo.style.display = "block";
    } else if (region != value) {
      countryInfo.style.display = "none";
    } else {
      countryInfo.style.display = "block";
    }
  }
}

